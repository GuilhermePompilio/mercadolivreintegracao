using System.ComponentModel.DataAnnotations;

namespace MercadoLivreIntegracao.Connection
{
    public class data
    {
        [Key]
        public int Id { get; set; }
        public string resource { get; set; }
        public string user_id { get; set; }
        public string topic { get; set; }
        public string application_id { get; set; }
        public string attempts { get; set; }
        public string sent { get; set; }
        public string received { get; set; }
    }
}