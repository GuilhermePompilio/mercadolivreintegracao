using Microsoft.EntityFrameworkCore;

namespace MercadoLivreIntegracao.Connection
{
    public class AppDBContext:DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {
        }

        public DbSet<data> mercadolivretable { get; set; }
    }
}