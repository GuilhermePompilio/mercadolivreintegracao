﻿using System.Collections.Generic;
using MercadoLivreIntegracao.Connection;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace MercadoLivreIntegracao.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MercadoLivreController : ControllerBase
    {
        private readonly AppDBContext db;

        public MercadoLivreController(AppDBContext db)
        {
            this.db = db;
        }

        [HttpGet("UrlML")]
        public string UrlML()
        {
            return "https://auth.mercadolivre.com.br/authorization?response_type=code&client_id=6217166373102029";
        }

        [HttpGet("CallBack")]
        public string CallBack(string code)
        {
            var client = new RestClient("https://api.mercadolibre.com/oauth/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);

            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("client_id", "6217166373102029");
            request.AddParameter("client_secret", "qsBHw6nGE35BUv29fgf3KokFxTZAstw7");
            request.AddParameter("code", code);
            request.AddParameter("redirect_uri", "https://localhost:5001/MercadoLivre/CallBack");

            IRestResponse rest = client.Execute(request);

            return rest.Content;
        }

        [HttpGet("RefreshToken")]
        public string RefreshToken(string refreshToken)
        {
            var client = new RestClient($"https://api.mercadolibre.com/oauth/token?grant_type=refresh_token&client_id=6217166373102029&client_secret=qsBHw6nGE35BUv29fgf3KokFxTZAstw7&refresh_token={refreshToken}");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);

            return response.Content;
        }

        [HttpPost("Notification")]
        public string Notification([FromBody] data obj)
        {
            db.Add(obj);
            db.SaveChanges();

            return "";
        }
    }
}
